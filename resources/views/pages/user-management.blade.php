@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav')
    <div class="row mt-4 mx-4">
        @if (strtolower($pageFor) == 'admin')
            @include('pages.admin.userManagement.userManagement')
        @elseif (strtolower($pageFor) == 'pic')
            @include('pages.admin.userManagement.userManagementPic')
        @elseif (strtolower($pageFor) == 'user')
            @include('pages.admin.userManagement.userManagementUser')
        @endif
    </div>
@endsection
