<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }
</style>
<div class="col-12">
    <div class="card">
        <form role="form" method="POST" action={{ route('genreSave') }} enctype="multipart/form-data"
            id="form-update-profile" style="display: block;">
            @csrf
            @method('POST')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Add Genre</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    <div class="d-flex justify-content-end ms-auto">
                        <span class="btn btn-danger btn-sm" id="button-to-cancel"
                            style="display:block; margin-right:2px" onclick="backToHome()">cancel</span>
                        <button type="submit" class="btn btn-primary btn-sm" id="button-to-edit"
                            style="display:block;margin-left:2px">Save</button>
                    </div>
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <p class="text-uppercase text-sm">Genre New Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Genre Type</label>
                            <input class="form-control" type="text" name="genreType" id="genreType" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Created By</label>
                            <input class="form-control" type="text" name="createdBy" value="{{ $fullName }}" id="createdBy" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">First name</label>
                                <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Last name</label>
                                <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                            </div>
                        </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Others Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Created At</label>
                            <input class="form-control" type="text" name="createdAt" value="{{ $dateNow }}"
                                id="createdAt" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Updated At</label>
                            <input class="form-control" type="text" name="updatedAt" value="{{ $dateNow }}"
                                id="updatedAt" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>


<script>
    function backToHome(){
        window.location.href = '/';
    }

</script>

