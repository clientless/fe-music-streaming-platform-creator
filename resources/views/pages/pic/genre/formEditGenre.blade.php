<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }
</style>
<div class="col-12">
    <div class="card">
        <form role="form" method="POST" action={{ route('genreFormEditSave') }} enctype="multipart/form-data"
            id="form-update-profile" style="display: block;">
            @csrf
            @method('PUT')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Edit Genre</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    <div class="d-flex justify-content-end ms-auto">
                        <span class="btn btn-danger btn-sm" id="button-to-cancel"
                            style="display:block; margin-right:2px" onclick="backToListGenre()">cancel</span>
                        <button type="submit" class="btn btn-primary btn-sm" id="button-to-edit"
                            style="display:block;margin-left:2px">Save</button>
                    </div>
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <p class="text-uppercase text-sm">Genre New Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">New Genre Type</label>
                            <input class="form-control" type="text" name="newGenreType" id="newGenreType" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Updated By</label>
                            <input class="form-control" type="text" name="updatedBy" id="updatedBy" required>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">First name</label>
                                <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Last name</label>
                                <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                            </div>
                        </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Genre Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Kode</label>
                            <input class="form-control" type="text" name="genreCode"
                                value="{{ $dataBody['genreCode'] }}" id="genreCode" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Name</label>
                            <input class="form-control" type="text" name="genreType"
                                value="{{ $dataBody['genreType'] }}" id="genreType" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Created At</label>
                            <input class="form-control" type="text" name="createdAt" value="{{ $dataBody['createdAt'] }}"
                                id="createdAt" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Updated At</label>
                            <input class="form-control" type="text" name="updatedAt" value="{{ $dataBody['updatedAt'] }}"
                                id="updatedAt" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function() {
        // Kode JavaScript Anda yang menggunakan sintaks jQuery
        $('#user-table').DataTable({
            searching: true,
            ordering: true,
            lengthMenu: [10, 25, 50, 75, 100],
            pageLength: 10,
            pagingType: 'full_numbers',
            language: {
                paginate: {
                    first: 'First',
                    last: 'Last',
                    next: 'Next',
                    previous: 'Previous'
                }
            }
        });
    });

    var selectedUsername = ''; // Variabel global untuk menyimpan username yang dipilih
    var selectedRole = ''; // Variabel global untuk menyimpan role yang dipilih

    function backToListGenre(){
        window.location.href = '/genre';
    }

    function toPageEditGenre(genreCode) {
        // selectedUsername = username;
        // selectedRole = role;
        window.location.href = '/genre/edit/' + genreCode;
    }

    function dataToDeleteGenre(username, role) {
        selectedUsername = username;
        selectedRole = role;
    }


    function nonActivateStatus() {
        console.log(selectedUsername);
        $.ajax({
            url: '/user-management/admin/non-activate-user', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'PATCH',
            data: {
                username: selectedUsername,
                role: selectedRole,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#accModal').modal('hide');
                if (response.status) {
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
                window.location.href = '/user-management/admin'
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#accModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/user-management/admin'


            }
        });
    }
</script>

<!-- Modal Non-Aktifkan -->
<div class="modal fade" id="accModal" tabindex="-1" aria-labelledby="accModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="accModalLabel">
                    Non-Aktifkan Status</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Apakah Anda yakin ingin non-aktifkan status akun
                ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary" onclick="nonActivateStatus()">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Pulihkan -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Pulihkan
                    Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Hubungi tim MSP-SUPPORT untuk melakukan pemulihan akun
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                {{-- <button type="button" class="btn btn-primary">Simpan</button> --}}
            </div>
        </div>
    </div>
</div>
