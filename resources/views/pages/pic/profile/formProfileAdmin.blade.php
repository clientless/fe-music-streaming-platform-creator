<div class="col-md-8">
    <div class="card">
        <form role="form" method="POST" action={{ route('profile.update') }} enctype="multipart/form-data"
            id="form-update-profile" style="display: block;">
            @csrf
            @method('PUT')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0">Edit Profile</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    <div class="d-flex justify-content-end ms-auto">
                        <span class="btn btn-primary btn-sm" id="button-to-view" style="display:block;"><i
                                class="fa fa-pen-fancy">&nbsp; edit</i></span>
                        <span class="btn btn-danger btn-sm" id="button-to-cancel"
                            style="display:none; margin-right:2px">cancel</span>
                        <button type="submit" class="btn btn-primary btn-sm" id="button-to-edit"
                            style="display:none;margin-left:2px">Save</button>
                    </div>
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <p class="text-uppercase text-sm">User Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Fullname</label>
                            <input class="form-control" type="text" name="fullname"
                                value="{{ $dataBody['fullname'] }}" id="fullname" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Email address</label>
                            <input class="form-control" type="email" name="email" value="{{ $dataBody['email'] }}"
                                id="email" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">First name</label>
                            <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Last name</label>
                            <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                        </div>
                    </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Work Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Username</label>
                            <input class="form-control" type="text" name="username"
                                value="{{ $dataBody['username'] }}" id="username" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Work Mail</label>
                            <input class="form-control" type="text" name="emailCompany"
                                value="{{ $dataBody['emailCompany'] }}" id="emailCompany" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Role</label>
                            <input class="form-control" type="text" name="role" value="{{ $dataBody['role'] }}"
                                id="role" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Status</label>
                            <input class="form-control" type="text" name="statusActive"
                                value="{{ $dataBody['status'] == true ? 'Aktif' : 'Non-Aktif' }}" id="statusActive"
                                readonly>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form role="form" method="POST" action={{ route('profile.update.password') }} enctype="multipart/form-data"
            id="form-update-password" style="display: none;">
            @csrf
            @method('PUT')
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0"><i class="fas fa-arrow-left" onclick="backToDetailProfile()"></i>&nbsp; Ubah
                        Password</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    <div class="d-flex justify-content-end ms-auto">
                        <button type="submit" class="btn btn-primary btn-sm"
                            style="display:block;margin-left:2px">Save</button>
                    </div>
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <p class="text-uppercase text-sm">User Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Work Mail</label>
                            <input class="form-control" type="text" name="emailCompany"
                                value="{{ $dataBody['emailCompany'] }}" id="emailCompany" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Password</label>
                            <input class="form-control" type="text" name="password" id="password" required>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
