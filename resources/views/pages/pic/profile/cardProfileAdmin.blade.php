<style>
    /* Styling for the profile card */
    .card-profile {
        width: 350px;
        margin: 0 auto;
        border-radius: 10px;
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        background-color: #ffffff;
        padding: 20px;
    }

    .card-profile img {
        width: 100%;
        border-radius: 10px 10px 0 0;
    }

    .card-profile .profile-image {
        width: 80px;
        height: 80px;
        border: 2px solid #fff;
    }

    /* Styling for the ID section */
    .id-section {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .id-section img {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        margin-right: 10px;
    }

    /* Styling for the download button */
    #downloadIdCard {
        width: 100%;
        padding: 10px 20px;
        background-color: #7095bc;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    #downloadIdCard:hover {
        background-color: #0056b3;
    }
</style>

<div class="col-md-4">
    <div class="card card-profile">
        <img src="/img/bg-profile.jpg" alt="Profile Background" class="card-img-top">
        <div class="row justify-content-center">
            <div class="col-4 col-lg-4 order-lg-2">
                <div class="mt-n4 mt-lg-n6 mb-4 mb-lg-0">
                    <a href="#">
                        <img src="/img/team-2.jpg"
                            class="rounded-circle img-fluid border border-2 border-white profile-image">
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="text-center">
                <h5 class="mb-1">
                    {{ $dataBody['fullname'] }}<span class="font-weight-light check-mark"><i
                            class="ni ni-check-bold text-success"></i></span>
                </h5>
                <div class="font-weight-300 mb-3">
                    <i class="ni location_pin mr-2"></i>{{ $dataBody['email'] }}
                </div>
                <div class="font-weight-500 mb-3">
                    <?php
                    $lenLetterRole = strlen($dataBody['role']);
                    $firstLetterRole = substr($dataBody['role'], 0, 1);
                    $nextLetterRole = substr(strtolower($dataBody['role']), 1, $lenLetterRole - 1);

                    $codeFooterCardExplode = explode('-', $dataBody['username']);
                    $codeFooterCard = $codeFooterCardExplode['1'];
                    ?>
                    <i class="ni business_briefcase-24 mr-2"></i>{{ $firstLetterRole . $nextLetterRole }} - MSP Support
                </div>
                <div class="font-weight-500">
                    <div class="id-section">
                        <img src="./assets/img/ToRing-Msp.png" alt="Image Company" class="profile-image">
                        ID{{ $dataBody['id'] . $codeFooterCard }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button id="downloadIdCard">Download Card</button>
</div>
<!-- Tambahkan script jsPDF -->



<script>
    // Pastikan jsPDF telah dimuat sebelum menjalankan fungsi downloadCardAsPDF
    function downloadCardAsPDF() {
        var dataBody = @json($dataBody);

        $.ajax({
            url: '/download-pdf',
            method: 'GET',
            data: {
                dataBody: dataBody,
            },
            success: function(response) {
                // Lakukan sesuatu dengan data yang diterima dari API
                // if (response.status) {
                //     console.log(response);
                // }
            },
            error: function(xhr, status, error) {
                // Tangani kesalahan jika panggilan API gagal
                console.error("error");
            }
        });

    }

    // Tambahkan event listener ke tombol unduh
    document.getElementById('downloadIdCard').addEventListener('click', downloadCardAsPDF);
</script>
