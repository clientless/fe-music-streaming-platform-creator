<div class="row">
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Genre</p>
                            <h5 class="font-weight-bolder">
                                {{-- {{ $totalGenre }} --}}
                            </h5>
                            <a href="{{ route('genre') }}">
                                <p class="mb-0">
                                    <span class="text-success text-sm font-weight-bolder"><i
                                            class="ni ni-fat-add"></i></span>
                                    <u>See All Genre</u>
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                            <i class="ni ni-note-03 text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Artist</p>
                            <h5 class="font-weight-bolder">
                                {{-- {{ $totalArtist }} --}}
                            </h5>
                            <a href="{{ route('artistLive') }}">
                                <p class="mb-0">
                                    <span class="text-success text-sm font-weight-bolder"><i
                                            class="ni ni-fat-add"></i></span>
                                    <u>See All Artist</u>
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-danger shadow-danger text-center rounded-circle">
                            <i class="ni ni-badge text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Album</p>
                            <h5 class="font-weight-bolder">
                                {{-- {{ $totalAlbum }} --}}
                            </h5>
                            <a href="{{ route('albumLive') }}">
                                <p class="mb-0">
                                    <span class="text-success text-sm font-weight-bolder"><i
                                            class="ni ni-fat-add"></i></span>
                                    <u>See All Album</u>
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-success shadow-success text-center rounded-circle">
                            <i class="ni ni-sound-wave text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-uppercase font-weight-bold">Total Song</p>
                            <h5 class="font-weight-bolder">
                                {{-- {{ $totalSong }} --}}
                            </h5>
                            <a href="{{ route('songLive') }}">
                                <p class="mb-0">
                                    <span class="text-success text-sm font-weight-bolder"><i
                                            class="ni ni-fat-add"></i></span>
                                    <u>See All Song</u>
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-warning shadow-warning text-center rounded-circle">
                            <i class="ni ni-button-play text-lg opacity-10" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-lg-7 mb-lg-0 mb-4">
        <div class="card z-index-2 h-100">
            <div class="card-header pb-0 pt-3 bg-transparent d-flex justify-content-between align-items-center">
                <div>
                    <h6 class="text-capitalize">Metriks Approval</h6>
                    <p class="text-sm mb-0">
                        <span id='logoMetriks'></span>
                        <span class="font-weight-bold" id="namaMetriks"></span>
                    </p>
                </div>
                <div class="d-flex align-items-center">
                    <span id='buttonMetriks'>

                    </span>
                </div>
            </div>
            <div class="card-body p-3">
                <div class="chart">
                    <canvas id="barChart" class="chart-canvas" height="100" width="300"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card card-carousel overflow-hidden h-100 p-0">
            <div id="carouselExampleCaptions" class="carousel slide h-100" data-bs-ride="carousel">
                <div class="carousel-inner border-radius-lg h-100">
                    <div class="carousel-item h-100 active"
                        style="background-image: url('./assets/img/guide.png');
    background-size: cover;">
                        <div class="carousel-caption d-none d-md-block bottom-0 text-start start-0 ms-5">
                            <div class="icon icon-shape icon-sm bg-white text-center border-radius-md mb-3">
                                <i class="ni ni-camera-compact text-dark opacity-10"></i>
                            </div>
                            <h5 class="text-white mb-1">Welcome In This Workspace</h5>
                            <p>Please click <b><u><a href="#" style="color: white">this</a></u></b> to read Guide
                                Instruction and Company Procedure.</p>
                        </div>
                    </div>
                    <div class="carousel-item h-100"
                        style="background-image: url('./assets/img/stakeholder-news.png');
    background-size: cover;">
                        <div class="carousel-caption d-none d-md-block bottom-0 text-start start-0 ms-5">
                            <div class="icon icon-shape icon-sm bg-white text-center border-radius-md mb-3">
                                <i class="ni ni-bulb-61 text-dark opacity-10"></i>
                            </div>
                            <h5 class="text-white mb-1">Know all company goals</h5>
                            <p>Please click <b><u><a href="#" style="color: white">this</a></u></b> to read Our
                                team and Goals.</p>
                        </div>
                    </div>
                    <div class="carousel-item h-100"
                        style="background-image: url('./assets/img/news.png');
    background-size: cover;">
                        <div class="carousel-caption d-none d-md-block bottom-0 text-start start-0 ms-5">
                            <div class="icon icon-shape icon-sm bg-white text-center border-radius-md mb-3">
                                <i class="ni ni-trophy text-dark opacity-10"></i>
                            </div>
                            <h5 class="text-white mb-1">Love from internal</h5>
                            <p>Please click <b><u><a href="#" style="color: white">this</a></u></b> to read
                                detail news.</p>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev w-5 me-3" type="button"
                    data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next w-5 me-3" type="button"
                    data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-lg-7 mb-lg-0 mb-4">
        <div class="card ">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                    <h6 class="mb-2">Album Waiting Approval</h6>
                    <a href="{{ route('albumWaitingLive') }}">
                        <h6 class="ms-2">View All <i class="ni ni-bold-right"></i></h6>
                    </a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center ">
                    <tbody>
                        <tr>
                            <td class="text-center">
                                No Data Yet
                            </td>
                        </tr>
                        {{-- @if (empty($listAlbumWaitingApproval))
                            <tr>
                                <td class="text-center">
                                    No Data Yet
                                </td>
                            </tr>
                        @else
                            @foreach ($listAlbumWaitingApproval as $listAlbumWaiting)
                                <tr>
                                    <td class="w-30">
                                        <div class="d-flex px-2 py-1 align-items-center">
                                            <div>
                                                <b>{{ $listAlbumWaiting['order_no'] }}</b>
                                            </div>
                                            <div class="ms-4">
                                                <a href="#">
                                                    <p class="text-xs font-weight-bold mb-0">Album:</p>
                                                    <h6 class="text-sm mb-0">{{ $listAlbumWaiting['titleAlbum'] }}</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="text-center">
                                            <p class="text-xs font-weight-bold mb-0">Artist:</p>
                                            <h6 class="text-sm mb-0">{{ $listAlbumWaiting['artistName'] }}</h6>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="text-center">
                                            <p class="text-xs font-weight-bold mb-0">Genre:</p>
                                            <h6 class="text-sm mb-0">{{ $listAlbumWaiting['genreType'] }}</h6>
                                        </div>
                                    </td>
                                    <td class="w-30">
                                        <div class="col text-center">
                                            <p class="text-xs font-weight-bold mb-0">&nbsp;</p>
                                            <a
                                                href="{{ URL('/album/waiting/live/detail/' . $listAlbumWaiting['idAlbum']) }}">
                                                <button class="btn btn-success btn-xs ms-auto">Detail</button>
                                            </a>
                                            <button class="btn btn-danger btn-xs ms-2">Reject</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">

                    <h6 class="mb-0">Artist Waiting Approval</h6>
                    <a href="{{ route('artistWaitingLive') }}">

                        <h6 class="ms-2">View All <i class="ni ni-bold-right"></i></h6>
                    </a>
                </div>

            </div>
            <div class="card-body p-3">
                <ul class="list-group">
                    <li class="text-center">
                        No Data Yet
                    </li>
                    {{-- @if (empty($listArtistWaitingApproval))
                        <li class="text-center">
                            No Data Yet
                        </li>
                    @else
                        @foreach ($listArtistWaitingApproval as $listWaitArtist)
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <div class="icon icon-shape icon-sm me-3 bg-gradient-light shadow text-center">
                                        {{ $listWaitArtist['order_no'] }}
                                    </div>
                                    <div class="d-flex flex-column">
                                        <a href="#">
                                            <h6 class="mb-1 text-dark text-sm">{{ $listWaitArtist['artistName'] }}
                                            </h6>
                                            <span class="text-xs">{{ substr($listWaitArtist['yearActive'], 0, 4) }},
                                                <span
                                                    class="font-weight-bold">{{ $listWaitArtist['genreType'] }}</span></span>

                                        </a>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <p class="text-xs font-weight-bold mb-0">&nbsp;</p>
                                    <a href="{{ URL('/artist/waiting/live/detail/' . $listWaitArtist['idArtist']) }}">
                                        <button class="btn btn-success btn-xs ms-auto">Detail</button>
                                    </a>
                                    <button class="btn btn-danger btn-xs ms-2">Reject</button>

                                    <button
                                        class="btn btn-link btn-icon-only btn-rounded btn-sm text-dark icon-move-right my-auto"><i
                                            class="ni ni-bold-right" aria-hidden="true"></i></button>
                                </div>
                            </li>
                        @endforeach
                    @endif --}}
                </ul>
            </div>
        </div>
    </div>
</div>
