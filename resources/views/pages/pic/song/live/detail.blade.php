<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }

    .song-image {
        border-radius: 20px;
        /* Membuat sudut gambar melengkung */
        object-fit: cover;
        /* Memastikan gambar terisi dengan baik dalam bingkai */
        width: 200px;
        /* Sesuaikan lebar gambar sesuai kebutuhan */
        height: 200px;
        /* Sesuaikan tinggi gambar sesuai kebutuhan */
    }

    .audio-player {
        text-align: center;
        margin-top: 50px;
    }

    .controls {
        margin-top: 20px;
    }

    button {
        margin: 0 5px;
        padding: 10px 20px;
    }
</style>
<div class="col-12">
    <div class="card">
        {{-- <form role="form" method="POST" action={{ route('approvalSongWaitingLiveDetailRejected') }} enctype="multipart/form-data" --}}
        <form role="form" enctype="multipart/form-data" id="form-update-profile" style="display: block;">
            {{-- @csrf --}}
            {{-- @method('PUT') --}}
            <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                    <p class="mb-0"><i class="fas fa-arrow-left" onclick="backToDetailProfile()"></i>&nbsp;Detail
                        Song</p>
                    {{-- <span > --}}
                    {{-- <span class="mb-5"> --}}
                    {{-- </span> --}}
                    {{-- </span> --}}
                </div>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <div class="form-group">
                        <img src="data:image/jpeg;base64,{{ $imgToBase64 }}" alt="Song Image"
                            class="img-fluid song-image">
                    </div>
                    <div class="audio-player">
                        <audio id="my-audio">Ito</audio>
                        <div class="controls">
                            <span id="play-btn" class="btn btn-outline-warning btn-sm ms-4">Play</span>
                            <span id="pause-btn" class="btn btn-outline-warning btn-sm ms-4">Pause</span>
                            <span id="stop-btn" class="btn btn-outline-warning btn-sm ms-4">Stop</span>
                        </div>
                    </div>

                </div>
                <p class="text-uppercase text-sm">Song Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">ID Song</label>
                            <input class="form-control" type="text" name="idSong" value="{{ $id }}"
                                id="idSong" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Artist</label>
                            <input class="form-control" type="text" name="artistName"
                                value="{{ $dataBody['artistName'] }}" id="artistName" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Title</label>
                            <input class="form-control" type="text" name="songName"
                                value="{{ $dataBody['titleSong'] }}" id="songName" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Duration</label>
                            <input class="form-control" type="text" name="durationSong"
                                value="{{ $dataBody['durationSong'] }}" id="durationSong" readonly>
                        </div>

                    </div>
                    {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">First name</label>
                                <input class="form-control" type="text" name="firstname"  value="{{ old('firstname', auth()->user()->firstname) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="form-control-label">Last name</label>
                                <input class="form-control" type="text" name="lastname" value="{{ old('lastname', auth()->user()->lastname) }}">
                            </div>
                        </div> --}}
                </div>
                <hr class="horizontal dark">
                <p class="text-uppercase text-sm">Others Information</p>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Album</label>
                            <input class="form-control" type="text" name="albumName"
                                value="{{ $dataBody['titleAlbum'] }}" id="albumName" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Language</label>
                            <input class="form-control" type="text" name="languageSong"
                                value="{{ $dataBody['language'] }}" id="languageSong" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Made By</label>
                            <input class="form-control" type="text" name="madeBy"
                                value="{{ empty($dataBody['madeBy']) ? '-' : $dataBody['madeBy'] }}" id="madeBy"
                                readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Streamed</label>
                            <input class="form-control" type="text" name="totalStreaming"
                                value="{{ $dataBody['totalStreaming'] }}" id="totalStreaming" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Year</label>
                            <input class="form-control" type="text" name="yearActive"
                                value="{{ $dataBody['releasedYear'] }}" id="yearActive" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Explicit</label>
                            <input class="form-control" type="text" name="explicit"
                                value="{{ $dataBody['explicit'] ? 'Yes' : 'No' }}" id="explicit" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Type Song</label>
                            <input class="form-control" type="text" name="songType"
                                value="{{ $dataBody['songType'] }}" id="songType" readonly>
                        </div>
                        <div class="form-group">
                            <label for="example-text-input" class="form-control-label">Downloaded</label>
                            <input class="form-control" type="text" name="totalDownload"
                                value="{{ $dataBody['totalDownload'] }}" id="totalDownload" readonly>
                        </div>
                    </div>

                </div>
                <div class="d-flex justify-content-end ms-auto">
                    <button type="button" class="btn btn-outline-warning btn-sm ms-4" id="button-to-back"
                        style="display:block; margin-right:2px" onclick="backToDetailProfile()">Back</button>
                </div>

            </div>

        </form>
    </div>

</div>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        var auth = @json($auth);
        var idSong = @json($id);
        // console.log("Beare"+auth);
        var audioPlayer = playAudioFromURL(idSong, auth);

        var playBtn = document.getElementById('play-btn');
        var pauseBtn = document.getElementById('pause-btn');
        var stopBtn = document.getElementById('stop-btn');

        playBtn.addEventListener('click', function() {
            audioPlayer.play();
        });

        pauseBtn.addEventListener('click', function() {
            audioPlayer.pause();
        });

        stopBtn.addEventListener('click', function() {
            audioPlayer.stop();
        });
    });

    function playAudioFromURL(idSong, auth) {
        var url = 'http://localhost:8081/v1/song/play_song/' + idSong
        var audio = document.getElementById('my-audio');
        var lastPausedTime = 0; // Menyimpan waktu terakhir ketika audio dijeda

        function play() {
            if (lastPausedTime > 0) {
                audio.currentTime = lastPausedTime;
                lastPausedTime = 0; // Mengatur kembali waktu terakhir ke 0 setelah dimulai kembali
            }
            audio.play();
        }

        function pause() {
            if (audio.paused) {
                audio.play();
            } else {
                audio.pause();
                lastPausedTime = audio.currentTime; // Menyimpan waktu saat ini dalam detik
            }
        }

        function stop() {
            audio.pause();
            audio.currentTime = 0;
            lastPausedTime = 0; // Mengatur kembali waktu terakhir ke 0 setelah dihentikan
        }

        // var headers = new Headers();
        // headers.append('Authorization', auth); // Ganti YOUR_ACCESS_TOKEN dengan token yang benar
        // console.log(headers['Authorization']);

        var headers = {
            'Authorization': auth,
            'urlexcp':'/v1/song/play_song/'
            // 'Accept':'*/*'
        }

        console.log(headers);

        fetch(url, {
                method: 'GET',
                Headers: headers,
                // mode: 'no-cors' // Set mode to 'no-cors'
            })
            .then(response => response.arrayBuffer())
            .then(dataByte => {
                var byteArray = new Uint8Array(dataByte);
                var blob = new Blob([byteArray], {
                    type: 'audio/mpeg'
                });
                var audioUrl = URL.createObjectURL(blob);

                audio.src = audioUrl;
            })
            .catch(error => {
                console.error('Error:', error);
            });

        return {
            play: play,
            pause: pause,
            stop: stop
        };
    }



    function backToHome() {
        window.location.href = '/';
    }

    function backToDetailProfile() {
        window.history.back();
    }
</script>
