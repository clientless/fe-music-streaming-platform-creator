<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }
</style>
<div class="col-12">
    <div class="card mb-4">
        <div class="card-header pb-0">
            <h6>List Artist Live</h6>
        </div>
        @php
            // dd($dataBody, session()->all());
        @endphp
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-3">
                <table id='user-table' class="table align-items-center mb-0 table-bordered">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Genre</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Year
                            </th>
                            {{-- <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                Status</th> --}}
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr> --}}
                        @if (empty($dataBody))
                        @else
                            @foreach ($dataBody as $dataBody)
                                <tr>
                                    <td>
                                        <div class="d-flex px-3 py-1">
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{ $dataBody['artistName'] }}</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex px-3 py-1">
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{ $dataBody['genreType'] }}</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex px-3 py-1">
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{ $dataBody['yearActive'] }}</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="align-middle text-end">
                                        <div class="d-flex px-3 py-1 justify-content-center align-items-center">
                                            <button class="btn btn-primary btn-sm" {{-- data-bs-toggle="modal" --}}
                                                {{-- data-bs-target="#accModal" --}}
                                                onclick="toPageDetailArtistLive('{{ $dataBody['idArtist'] }}')">Detail
                                                Artist</button>

                                            <button class="btn btn-outline-warning btn-sm ms-4" data-bs-toggle="modal"
                                                data-bs-target="#deleteModal"
                                                onclick="dataToDeleteArtistLive('{{ $dataBody['idArtist'] }}')">Hapus
                                                Artist</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        {{-- </tr> --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function() {
        // Kode JavaScript Anda yang menggunakan sintaks jQuery
        $('#user-table').DataTable({
            searching: true,
            ordering: true,
            lengthMenu: [10, 25, 50, 75, 100],
            pageLength: 10,
            pagingType: 'full_numbers',
            language: {
                paginate: {
                    first: 'First',
                    last: 'Last',
                    next: 'Next',
                    previous: 'Previous'
                }
            }
        });
    });

    var selectedUsername = ''; // Variabel global untuk menyimpan username yang dipilih
    var selectedRole = ''; // Variabel global untuk menyimpan role yang dipilih
    var selectedGenreCode = ''; // Variabel global untuk menyimpan username yang dipilih
    var selectedIdArtist = ''; // Variabel global untuk menyimpan username yang dipilih


    function toPageDetailArtistLive(idArtist) {
        // selectedUsername = username;
        // selectedRole = role;
        window.location.href = '/artist/live/detail/' + idArtist;
    }

    function dataToDeleteArtistLive(idArtist) {
        // selectedUsername = username;
        selectedIdArtist = idArtist;
    }


    function nonActivateStatus() {
        var reasonField = document.getElementById('reasonFieldDelete').value;
        var deletedBy = @json(session()->all()['userinfo']['username']);
        console.log(reasonField);
        // console.log(selectedUsername);
        $.ajax({
            url: '/artist/live/delete', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'DELETE',
            data: {
                // username: selectedUsername,
                idArtist: selectedIdArtist,
                reasonField: reasonField,
                deletedBy: deletedBy,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#deleteModal').modal('hide');
                if (response.status) {
                    toastr.success(response.msg);
                } else {
                    toastr.error(response.msg);
                }
                window.location.href = '/artist/live';
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#deleteModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/artist/live';


            }
        });
    }
</script>

<!-- Modal Pulihkan -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Anda akan menghapus Artist ini dari List dengan alasan di bawah:
                <div>
                    <textarea class="form-control" style="min-width: 100%" id='reasonFieldDelete' name="reasonFieldDelete" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="nonActivateStatus()">Save</button>
            </div>
        </div>
    </div>
</div>
