<style>
    /* Menjadikan garis border antar data menjadi transparan */
    #user-table tbody tr td {
        border-color: transparent;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000000 !important;
        border: 1px solid #FF7F50;
        background-color: rgb(98, 56, 56);
        background: linear-gradient(to bottom, white 0%, #FF7F50 100%);
    }
</style>
<div class="col-12">
    <div class="card mb-4">
        <div class="card-header pb-0">
            <h6>List User {{ $pageFor }}</h6>
        </div>
        @php
            // dd($dataBody);
        @endphp
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-3">
                <table id='user-table' class="table align-items-center mb-0 table-bordered">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kode</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Role
                            </th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                Status</th>
                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr> --}}
                        @if (empty($dataBody))
                        @else
                            @foreach ($dataBody as $dataBody)
                                @if ($dataBody['username'] == session()->all()['userinfo']['username'])
                                @else
                                    <tr>
                                        <td>
                                            <div class="d-flex px-3 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $dataBody['username'] }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex px-3 py-1">
                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $dataBody['fullname'] }}</h6>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $dataBody['role'] }}</p>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <p class="text-sm font-weight-bold mb-0">
                                                {{ $dataBody['status'] ? 'Aktif' : 'Tidak Aktif' }}</p>
                                        </td>
                                        <td class="align-middle text-end">
                                            <div class="d-flex px-3 py-1 justify-content-center align-items-center">
                                                @if ($dataBody['status'])
                                                    <button class="btn btn-primary btn-sm" data-bs-toggle="modal"
                                                        data-bs-target="#nonAktifkanModal"
                                                        onclick="tempValueNonActivateStatus('{{ $dataBody['username'] }}', '{{ $dataBody['role'] }}')">Non-Aktifkan
                                                        Status</button>
                                                @else
                                                    <button class="btn btn-outline-warning btn-sm"
                                                        data-bs-toggle="modal" data-bs-target="#pulihkanModal"
                                                        onclick="tempValueActivateStatus('{{ $dataBody['username'] }}', '{{ $dataBody['role'] }}')">Pulihkan
                                                        Akun</button>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        {{-- </tr> --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function() {
        // Kode JavaScript Anda yang menggunakan sintaks jQuery
        $('#user-table').DataTable({
            searching: true,
            ordering: true,
            lengthMenu: [10, 25, 50, 75, 100],
            pageLength: 10,
            pagingType: 'full_numbers',
            language: {
                paginate: {
                    first: 'First',
                    last: 'Last',
                    next: 'Next',
                    previous: 'Previous'
                }
            }
        });
    });

    var selectedUsername = ''; // Variabel global untuk menyimpan username yang dipilih
    var selectedRole = ''; // Variabel global untuk menyimpan role yang dipilih


    function tempValueNonActivateStatus(username, role) {
        selectedUsername = username;
        selectedRole = role;
    }

    function tempValueActivateStatus(username, role) {
        selectedUsername = username;
        selectedRole = role;
    }


    function nonActivateStatus() {
        console.log(selectedUsername);
        $.ajax({
            url: '/user-management/user/non-activate-user', // Ganti '/url/endpoint' dengan URL endpoint Anda
            type: 'PATCH',
            data: {
                username: selectedUsername,
                role: selectedRole,
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                // Tanggapan dari server
                $('#nonAktifkanModal').modal('hide');
                if (response.status) {
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
                window.location.href = '/user-management/user'
            },
            error: function(xhr, status, error) {
                // Tanggapan error dari server
                $('#nonAktifkanModal').modal('hide');
                console.error('Error:', error);
                toastr.error('Error: ' + error);
                window.location.href = '/user-management/user'


            }
        });
    }
</script>

<!-- Modal Non-Aktifkan -->
<div class="modal fade" id="nonAktifkanModal" tabindex="-1" aria-labelledby="nonAktifkanModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="nonAktifkanModalLabel">
                    Non-Aktifkan Status</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Apakah Anda yakin ingin non-aktifkan status akun
                ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary" onclick="nonActivateStatus()">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Pulihkan -->
<div class="modal fade" id="pulihkanModal" tabindex="-1" aria-labelledby="pulihkanModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="pulihkanModalLabel">Pulihkan
                    Akun</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Hubungi tim MSP-SUPPORT untuk melakukan pemulihan akun
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                {{-- <button type="button" class="btn btn-primary">Simpan</button> --}}
            </div>
        </div>
    </div>
</div>
