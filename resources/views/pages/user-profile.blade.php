@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav')
    <div class="card shadow-lg mx-4 card-profile-bottom">
        @if ($dataBody['role'] == 'ADMIN')
            @include('pages.admin.profile.briefProfileAdmin')
        @endif
    </div>
    <div id="alert">
        @include('components.alert')
    </div>
    <div class="container-fluid py-4">
        @if ($dataBody['role'] == 'ADMIN')
            <div class="row">
                @include('pages.admin.profile.formProfileAdmin')
                @include('pages.admin.profile.cardProfileAdmin')
            </div>
        @endif
    </div>
    @include('layouts.footers.auth.footer')
@endsection
@push('js')
    <script>
        var formUpdate = document.getElementById('form-update-profile');
        var formUpdatePassword = document.getElementById('form-update-password');
        var buttonType = document.getElementById('button-to-edit');
        var buttonTypeView = document.getElementById('button-to-view');
        var buttonTypeCancel = document.getElementById('button-to-cancel');
        var fullnameField = document.getElementById('fullname');
        var emailField = document.getElementById('email');
        var usernameField = document.getElementById('username');
        var workMailField = document.getElementById('emailCompany');
        var roleField = document.getElementById('role');
        var statusActiveField = document.getElementById('statusActive');
        var initiateStatus = false;

        // buttonType.innerHTML = '<i class="fa fa-pen-fancy">&nbsp; Edit</i> ';


        buttonTypeCancel.addEventListener("click", function(event) {
            setAtrributeFieldToOnlyView();
        });

        buttonTypeView.addEventListener("click", function(event) {
            setAttrubuteFieldToEdit();
            // fullnameField.focus();
            // emailField.focus();

            // inputField.focus();
        });

        function backToDetailProfile(){
            setAtrributeFieldToOnlyView();
        }

        function toPageResetPassword(){
            formUpdate.style.display = 'none';
            formUpdatePassword.style.display = 'block';
        }

        function setAtrributeFieldToOnlyView() {
            // initiateStatus = false;
            // buttonType.innerHTML = '<i class="fa fa-pen-fancy">&nbsp; Edit</i>';
            formUpdate.style.display = 'block';
            formUpdatePassword.style.display = 'none';
            buttonType.style.display = "none";
            buttonTypeCancel.style.display = "none";
            buttonTypeView.style.display = "block";
            fullnameField.setAttribute("readonly", "readonly");
            emailField.setAttribute("readonly", "readonly");
            usernameField.setAttribute("readonly", "readonly");
            workMailField.setAttribute("readonly", "readonly");
            roleField.setAttribute("readonly", "readonly");
            statusActiveField.setAttribute("readonly", "readonly");
        }

        function setAttrubuteFieldToEdit() {
            // initiateStatus = true;
            formUpdate.style.display = 'block';
            formUpdatePassword.style.display = 'none';
            buttonTypeView.style.display = "none";
            buttonType.style.display = "block";
            buttonTypeCancel.style.display = "block";
            fullnameField.removeAttribute("readonly");
            emailField.removeAttribute("readonly");
            // usernameField.removeAttribute("readonly");
            // workMailField.removeAttribute("readonly");
            // roleField.removeAttribute("readonly");
            // statusActiveField.removeAttribute("readonly");

        }
    </script>
@endpush
