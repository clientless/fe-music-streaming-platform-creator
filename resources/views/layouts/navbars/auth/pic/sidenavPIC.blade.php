<hr class="horizontal dark mt-0">

{{-- <div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main"> --}}
<div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteName() == 'home' ? 'active' : '' }}" href="{{ route('home') }}">
                <div
                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="ni ni-tv-2 text-primary text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Dashboard</span>
            </a>
        </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">User Pages</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteName() == 'profile' ? 'active' : '' }}"
                href="{{ route('profile') }}">
                <div
                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="ni ni-single-02 text-dark text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Profile</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="dropdown" id="dropdownGenre">
                <div
                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="ni ni-bullet-list-67 text-dark text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">User Management</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownGenre">
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('user.management.pic') }}">
                        <i class="fa-solid fa-user-tie text-danger text-sm opacity-10"></i>
                        &nbsp; Pic
                    </a>
                </li>
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('user.management.pic') }}">
                        <i class="fa-solid fa-user-tie text-warning text-sm opacity-10"></i>
                        &nbsp; PIC
                    </a>
                </li>
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('user.management.user') }}">
                        <i class="fa-solid fa-user-tie text-success text-sm opacity-10"></i>
                        &nbsp; User
                    </a>
                </li>

            </ul>

        </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Routine Pages</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="dropdown" id="dropdownGenre">
                <div
                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="ni ni-note-03 text-warning text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Genre</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownGenre">
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('genre') }}">
                        <i class="ni ni-tv-2 text-warning text-sm opacity-10"></i>
                        &nbsp; Live
                    </a>
                </li>
                <li class="nav-link-text ms-5" style="display: block">
                    <a class="dropdown-item" href="{{ route('genreForm') }}">
                        <i class="fas fa-plus text-warning text-sm opacity-10"></i>
                        &nbsp;
                        Add New
                    </a>
                </li>
                <li class="nav-link-text ms-5" style="display: none">
                    <a class="dropdown-item" href="#">
                        <i class="ni ni-key-25 text-warning text-sm opacity-10"></i>
                        &nbsp;
                        Deleted
                    </a>
                </li>

            </ul>

        </li>

        <li class="nav-item">

            <a class="nav-link" data-bs-toggle="dropdown" id="dropdownAlbum">
                <div
                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="ni ni-sound-wave text-success text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Album</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownAlbum">
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{route('albumLive')}}">
                        <i class="ni ni-tv-2 text-warning text-sm opacity-10"></i>
                        &nbsp; Live
                    </a>
                </li>
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('albumWaitingLive') }}">
                        <i class="ni ni-key-25 text-warning text-sm opacity-10"></i>
                        &nbsp;
                        Waiting Approve
                    </a>
                </li>

            </ul>

        </li>

        <li class="nav-item">


            <a class="nav-link" data-bs-toggle="dropdown" id="dropdownAlbum">
                <div
                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="ni ni-badge text-info text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Artist</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownAlbum">
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('artistLive') }}">
                        <i class="ni ni-tv-2 text-warning text-sm opacity-10"></i>
                        &nbsp; Live
                    </a>
                </li>
                <li class="nav-link-text ms-5">
                    <a class="dropdown-item" href="{{ route('artistWaitingLive') }}">
                        <i class="ni ni-key-25 text-warning text-sm opacity-10"></i>
                        &nbsp;
                        Waiting Approve
                    </a>
                </li>

            </ul>


        </li>




    </ul>

</div>
