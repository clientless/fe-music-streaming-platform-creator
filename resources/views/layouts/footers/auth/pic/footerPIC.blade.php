<div class="col-lg-6 mb-lg-0 mb-4">
    <div class="copyright text-center text-sm text-muted text-lg-start">
        ©
        <script>
            document.write(new Date().getFullYear())
        </script>, customize by MSP Support Team
        {{-- <a href="#" class="font-weight-bold" target="_blank">Lukito Andriansyah</a>
        &
        <a href="#" class="font-weight-bold" target="_blank">Arine Fitriani</a> --}}
        for an Creator Usage
    </div>
</div>
