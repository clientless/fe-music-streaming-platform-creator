<footer class="footer pt-3  mt-auto">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
            @if (isset(session()->all()['userinfo']))
                @if (session()->all()['userinfo']['role'] == 'PIC')
                    @include('layouts.footers.auth.pic.footerPic')
                @elseif(session()->all()['userinfo']['role'] == 'PIC')
                @include('layouts.footers.auth.pic.footerPIC')
                @elseif(session()->all()['userinfo']['role'] == 'USER')
                @include('layouts.footers.auth.user.footerUser')
                @endif
            @else
            @endif


            <div class="col-lg-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="#" class="nav-link text-muted"
                            target="_blank">About Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
