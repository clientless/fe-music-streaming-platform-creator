<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ArtistController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\ResetPassword;
use App\Http\Controllers\ChangePassword;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\PDFController\PDFController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\UserManagement;

Route::get('/', function () {
    return redirect('/dashboard');
})->middleware('PIC');
Route::get('/register', [RegisterController::class, 'create'])->middleware('guest')->name('register');
Route::post('/register', [RegisterController::class, 'store'])->middleware('guest')->name('register.perform');
Route::get('/login', [LoginController::class, 'show'])->middleware('guest')->name('login');
Route::post('/login', [LoginController::class, 'login'])->middleware('guest')->name('login.perform');
Route::get('/reset-password', [ResetPassword::class, 'show'])->middleware('guest')->name('reset-password');
Route::post('/reset-password', [ResetPassword::class, 'send'])->middleware('guest')->name('reset.perform');
Route::get('/change-password', [ChangePassword::class, 'show'])->middleware('guest')->name('change-password');
Route::post('/change-password', [ChangePassword::class, 'update'])->middleware('guest')->name('change.perform');
Route::get('/dashboard', [HomeController::class, 'index'])->name('home')->middleware('PIC');
Route::group(['middleware' => 'PIC'], function () {
    Route::get('/virtual-reality', [PageController::class, 'vr'])->name('virtual-reality');
    Route::get('/rtl', [PageController::class, 'rtl'])->name('rtl');
    Route::get('/download-pdf', [PDFController::class, 'downloadPDF']);
    Route::get('/profile-static', [PageController::class, 'profile'])->name('profile-static');
    Route::get('/sign-in-static', [PageController::class, 'signin'])->name('sign-in-static');
    Route::get('/sign-up-static', [PageController::class, 'signup'])->name('sign-up-static');


    //Profile
    Route::get('/profile', [UserProfileController::class, 'show'])->name('profile');
    Route::put('/profile', [UserProfileController::class, 'update'])->name('profile.update');
    Route::put('/profile/update/password', [UserProfileController::class, 'updatePassword'])->name('profile.update.password');


    //User Management
    Route::get('/user-management/pic', [UserManagement::class, 'showPic'])->name('user.management.pic');
    Route::patch('/user-management/pic/non-activate-user', [UserManagement::class, 'nonActivatePic'])->name('user.management.pic.nonactivate.user');
    Route::get('/user-management/pic', [UserManagement::class, 'showPic'])->name('user.management.pic');
    Route::patch('/user-management/pic/non-activate-user', [UserManagement::class, 'nonActivatePic'])->name('user.management.pic.nonactivate.user');
    Route::get('/user-management/user', [UserManagement::class, 'showUser'])->name('user.management.user');
    Route::patch('/user-management/user/non-activate-user', [UserManagement::class, 'nonActivateUser'])->name('user.management.user.nonactivate.user');

    //Genre
    Route::get('/genre', [GenreController::class, 'showGenre'])->name('genre');
    Route::get('/genre/edit/{idGenre}', [GenreController::class, 'showFormEditGenre'])->name('genreFormEdit');
    Route::put('/genre/edit/save', [GenreController::class, 'saveFormEditGenre'])->name('genreFormEditSave');
    Route::delete('/genre/delete/save', [GenreController::class, 'saveDeleteGenre'])->name('genreDeleteSave');
    Route::get('/genre/form', [GenreController::class, 'showFormGenre'])->name('genreForm');
    Route::post('/genre/save', [GenreController::class, 'saveGenre'])->name('genreSave');

    //Artist
    Route::get('/artist/live', [ArtistController::class, 'showArtistLive'])->name('artistLive');
    Route::delete('/artist/live/delete', [ArtistController::class, 'artistLiveDelete'])->name('artistLiveDelete');
    Route::get('/artist/live/detail/{id}', [ArtistController::class, 'showArtistLiveDetail'])->name('artistLiveDetail');
    Route::get('/artist/waiting/live', [ArtistController::class, 'showArtistWaitingLive'])->name('artistWaitingLive');
    Route::get('/artist/waiting/live/detail/{id}', [ArtistController::class, 'showArtistWaitingLiveDetail'])->name('artistWaitingLiveDetail');
    Route::put('/artist/waiting/live/detail/approval/rejected', [ArtistController::class, 'approvalArtistWaitingLiveDetailRejected'])->name('approvalArtistWaitingLiveDetailRejected');
    Route::put('/artist/waiting/live/detail/approval/approved', [ArtistController::class, 'approvalArtistWaitingLiveDetailApproved'])->name('approvalArtistWaitingLiveDetailApproved');

    //Album
    Route::get('/album/live', [AlbumController::class, 'showAlbumLive'])->name('albumLive');
    Route::delete('/album/live/delete', [AlbumController::class, 'albumLiveDelete'])->name('albumLiveDelete');
    Route::get('/album/live/detail/{id}', [AlbumController::class, 'showAlbumLiveDetail'])->name('albumLiveDetail');
    Route::get('/album/waiting/live', [AlbumController::class, 'showAlbumWaitingLive'])->name('albumWaitingLive');
    Route::get('/album/waiting/live/detail/{id}', [AlbumController::class, 'showAlbumWaitingLiveDetail'])->name('albumWaitingLiveDetail');
    Route::put('/album/waiting/live/detail/approval/rejected', [AlbumController::class, 'approvalAlbumWaitingLiveDetailRejected'])->name('approvalAlbumWaitingLiveDetailRejected');
    Route::put('/album/waiting/live/detail/approval/approved', [AlbumController::class, 'approvalAlbumWaitingLiveDetailApproved'])->name('approvalAlbumWaitingLiveDetailApproved');

    //Song
    Route::get('/song/live', [SongController::class, 'showSongLive'])->name('songLive');
    Route::delete('/song/live/delete', [SongController::class, 'songLiveDelete'])->name('songLiveDelete');
    Route::get('/song/live/detail/{id}', [SongController::class, 'showSongLiveDetail'])->name('songLiveDetail');
    // Route::get('/album/waiting/live', [AlbumController::class, 'showAlbumWaitingLive'])->name('albumWaitingLive');
    Route::get('/song/waiting/live/detail/{id}', [SongController::class, 'showSongWaitingLiveDetail'])->name('songWaitingLiveDetail');
    // Route::put('/album/waiting/live/detail/approval/rejected', [AlbumController::class, 'approvalAlbumWaitingLiveDetailRejected'])->name('approvalAlbumWaitingLiveDetailRejected');
    // Route::put('/album/waiting/live/detail/approval/approved', [AlbumController::class, 'approvalAlbumWaitingLiveDetailApproved'])->name('approvalAlbumWaitingLiveDetailApproved');





    Route::get('/{page}', [PageController::class, 'index'])->name('page');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
});
