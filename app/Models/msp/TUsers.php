<?php

namespace App\Models\MSP;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;


class TUsers extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = [
        'id',
        'username',
        'email',
        'password',
        'role',
        'pk_id_source',
        'status',
        'created_at',
        'updated_at'
    ];



    public static function insertToTableUsers($responseApi, $request)
    {
        $saveDataUsers = new TUsers();

        $saveDataUsers->username = $responseApi['username'];
        $saveDataUsers->email = $responseApi['email'];
        $saveDataUsers->role = $responseApi['role'];
        $saveDataUsers->pk_id_source = $responseApi['id'];
        $saveDataUsers->status = $responseApi['status'];
        $saveDataUsers->password = bcrypt($request->password);
        // dd($saveDataUsers);

        $insert = $saveDataUsers->save();

        if (!$insert) {
            $error = "err";
            Log::info($error);
        }
    }

    public static function updateToTableUsers($responseApi, $request)
    {
        $saveDataUsers = TUsers::where('pk_id_source', '=', $responseApi['id'])->where('role', '=', $responseApi['role'])->first();
        if ($saveDataUsers) {

            $saveDataUsers->email = $responseApi['email'];
            $insert = $saveDataUsers->save();

            if (!$insert) {
                $error = "err";
                Log::info($error);
            }
        }
    }

    public static function updatePasswordToTableUsers($responseApi, $request)
    {
        $saveDataUsers = TUsers::where('pk_id_source', '=', $responseApi['id'])->where('role', '=', session()->all()['userinfo']['role'])->first();
        if ($saveDataUsers) {
            $saveDataUsers->password = bcrypt($request->password);
            $insert = $saveDataUsers->save();

            if (!$insert) {
                $error = "err";
                Log::info($error);
            }
        }
    }

    public static function updateStatusNonActiveToTableUsers($responseApi, $request)
    {
        $saveDataUsers = TUsers::where('pk_id_source', '=', $responseApi['id'])->where('role', '=', $responseApi['role'])->first();
        if ($saveDataUsers) {
            $saveDataUsers->status = false;
            $insert = $saveDataUsers->save();

            if (!$insert) {
                $error = "err";
                Log::info($error);
            }
        }
    }




    public function created_at()
    {
        return $this->date($this->created_at);
    }

    public function updated_at()
    {
        return $this->date($this->updated_at);
    }
}
