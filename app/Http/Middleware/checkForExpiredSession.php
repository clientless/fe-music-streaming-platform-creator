<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\Auth;

class CheckForExpiredSession extends StartSession
{
    public function handle($request, Closure $next)
    {
        // Pengecekan sesi apakah telah berakhir
        if (Auth::check() && now()->greaterThan(auth()->user()->session_expire_at)) {
            event(new \App\Events\LogoutUser(auth()->id()));
            Auth::logout();
        }

        return $next($request);
    }
}
