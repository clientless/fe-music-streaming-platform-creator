<?php

namespace App\Http\Middleware;

use Closure;

class CheckLoggedInStatus
{
    public function handle($request, Closure $next)
    {
        // Memeriksa apakah pengguna masih masuk
        if (auth()->check() && auth()->user()->is_logged_in) {
            // Tandai pengguna sebagai tidak masuk
            auth()->user()->update(['is_logged_in' => false]);

            // Logout pengguna secara manual
            auth()->logout();

            // Redirect atau respon sesuai dengan kebijakan Anda
            return redirect('/login')->with('error', 'Your account is being used on another device.');
        }

        return $next($request);
    }
}
