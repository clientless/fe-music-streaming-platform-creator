<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Password;

class LoginController extends Controller
{
    /**
     * Display login page.
     *
     * @return Renderable
     */
    public function show()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);



        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            if (auth()->user()->is_logged_in) {
                Auth::logout();
                return redirect('/login')->with('error', 'Your account is being used on another device.');
            }

            $request->session()->regenerate();
            auth()->user()->update(['is_logged_in' => true]);

            //HitApiLogin
            try {
                $datas = [
                    'username' => $request->username,
                    'password' => $request->password,
                ];

                $response = Http::withBody(json_encode($datas), 'application/json')
                    ->post('http://localhost:8082/login');
                $guzzleResponse = $response->toPsrResponse();
                $data = $response->json();

                $userinfo = $data['info'];
                $token = $data['token'];
                $keyAuthorization = 'Authorization';
                $statusCode = $guzzleResponse->getStatusCode();

                if ($statusCode == 200 && $guzzleResponse->hasHeader($keyAuthorization)) {
                    session()->put('userinfo', $userinfo);
                    session()->put('token', $token);
                    session()->put('keyAuth', $keyAuthorization);
                    return redirect()->intended('dashboard');
                }
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                return redirect()->route('login')->withSuccess('Oppes! You have entered invalid credentials');
            }
        }

        return back()->withErrors([
            'username' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
