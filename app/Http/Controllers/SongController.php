<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SongController extends Controller
{
    //
    public function showSongLive()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Song Live');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $listSongLive = self::listSongLive($auth);

        $message = $listSongLive['message'];
        $statusCode = $listSongLive['code'];
        $dataBody = $listSongLive['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'success to get list data');
        if ($status) {
            return view('pages.songLive', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function showSongLiveDetail($id)
    {

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Song Live Detail');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);
        $songLiveDetail = self::detailSongLive($auth, $id);
        $imgSong = self::imgSongLive($auth, $id);

        $message = $songLiveDetail['message'];
        $statusCode = $songLiveDetail['code'];
        $dataBody = $songLiveDetail['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get data');
        if ($status) {
            $imgToBase64 = base64_encode($imgSong);
            // dd($dataBody);
            return view('pages.songLiveDetail', compact('dataBody', 'imgToBase64','id','auth'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function songLiveDelete(Request $request)
    {
        $requestBody = $request->all();

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'song Live Delete');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);

        try {
            $songLiveDetail = self::getSongLiveDelete($auth,$requestBody);

            $message = $songLiveDetail['message'];
            $statusCode = $songLiveDetail['code'];
            $dataBody = $songLiveDetail['data'];
            $status = ($statusCode == 200 && strtolower($message) == 'successfully to deleted');
            // dd($status);
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Delete User. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }

    public function showSongWaitingLiveDetail($id)
    {

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Song Waiting Live Detail');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);
        $songLiveDetail = self::detailSongWaitingLive($auth, $id);
        $imgSong = self::imgSongWaitingLive($auth, $id);

        $message = $songLiveDetail['message'];
        $statusCode = $songLiveDetail['code'];
        $dataBody = $songLiveDetail['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get data');
        if ($status) {
            $imgToBase64 = base64_encode($imgSong);
            // dd($dataBody);
            return view('pages.songWaitingLiveDetail', compact('dataBody', 'imgToBase64','id','auth'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }



    private static function listSongLive($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/song/list_song';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();

        return $jsonResponse;
    }


    private static function detailSongLive($authorizationUser, $idSong)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/song/view_song/' . $idSong;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function imgSongLive($authorizationUser, $idSong)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/song/view_cover_song/' . $idSong;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'image/jpeg',
        ])->get($url);

        $bodyResponse = $response->body();

        return $bodyResponse;
    }

    private static function detailSongWaitingLive($authorizationUser, $idSong)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_song/view_song/' . $idSong;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function imgSongWaitingLive($authorizationUser, $idSong)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_song/view_cover_song/' . $idSong;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'image/jpeg',
        ])->get($url);

        $bodyResponse = $response->body();

        return $bodyResponse;
    }


    private static function getSongLiveDelete($authorizationUser, $request)
    {
        $requestBody = $request;
        // dd($requestBody);

        $idsong = $requestBody['idSong'];
        $token = $authorizationUser;
        // dd($token);
        $url = 'http://localhost:8081/v1/song/delete/by/' . $idsong;
        $datas = ['deletedBy' => $requestBody['deletedBy'], 'reasonDeleted' => $requestBody['reasonField']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->delete($url, $datas);



        $jsonResponse = $response->json();
        // dd($response)

        return $jsonResponse;
    }



}
