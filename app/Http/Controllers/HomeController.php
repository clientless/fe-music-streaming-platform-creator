<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (!isset(session()->all()['userinfo'])) {
            session()->flush();
            Log::info("User Tidak Bisa login karena status user tidak aktif");
            return redirect()->to('/login');
        }

        $sessionUser = session()->all();
        $userInfo = $sessionUser['userinfo'];
        $keyAuth = $sessionUser['keyAuth'];
        $tokenApi = $sessionUser['token'];
        $roleUser = $userInfo['role'];
        $userName = $userInfo['username'];
        $authorizationUser = $tokenApi[$keyAuth];

        //totalGenre
        // $totalGenre = self::countTotalGenre($authorizationUser);
        //totalAlbum
        // $totalAlbum = self::countTotalAlbum($authorizationUser);
        //totalArtist
        // $totalArtist = self::countTotalArtist($authorizationUser);
        //totalLagu
        // $totalSong = self::countTotalSong($authorizationUser);

        //waitingApprovalAlbum
        // $listAlbumWaitingApproval = self::listAlbumWaitingApproval($authorizationUser);
        // $totalAlbumWaitingApproval = self::countTotalAlbumWaitingApproval($authorizationUser);
        // $totalAlbumWaitingApprovalIsApproved = self::countTotalAlbumWaitingApprovalIsApproved($authorizationUser);
        // $totalAlbumWaitingApprovalIsRejected = self::countTotalAlbumWaitingApprovalIsRejected($authorizationUser);

        //waitingApprovalArtist
        // $listArtistWaitingApproval = self::listArtistWaitingApproval($authorizationUser);
        // $totalArtistWaitingApproval = self::countTotalArtistWaitingApproval($authorizationUser);
        // $totalArtistWaitingApprovalIsApproved  = self::countTotalArtistWaitingApprovalIsApproved($authorizationUser);
        // $totalArtistWaitingApprovalIsRejected = self::countTotalArtistWaitingApprovalIsRejected($authorizationUser);

        session()->put('page', 'Dashboard');


        return view('pages.dashboard', compact(
            'sessionUser',
            'userInfo',
            'keyAuth',
            'tokenApi',
            'roleUser',
            'userName',
            'authorizationUser',
            // 'totalGenre',
            // 'totalAlbum',
            // 'totalArtist',
            // 'totalSong',
            // 'totalAlbumWaitingApproval',
            // 'totalAlbumWaitingApprovalIsApproved',
            // 'totalAlbumWaitingApprovalIsRejected',
            // 'totalArtistWaitingApproval',
            // 'totalArtistWaitingApprovalIsApproved',
            // 'totalArtistWaitingApprovalIsRejected',
            // 'listAlbumWaitingApproval',
            // 'listArtistWaitingApproval',
        ));
        // return view('pages.dashboard');

    }

    private static function countTotalGenre($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/genre/list';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            return count($response->json()['data']);
        } else {
            return 0;
        }
    }

    private static function countTotalArtist($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            return count($response->json()['data']);
        } else {
            return 0;
        }
    }

    private static function countTotalAlbum($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            return count($response->json()['data']);
        } else {
            return 0;
        }
    }

    private static function countTotalSong($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/song/list_song';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            return count($response->json()['data']);
        } else {
            return 0;
        }
    }

    private static function countTotalAlbumWaitingApproval($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            return count($response->json()['data']);
        } else {
            return 0;
        }
    }

    private static function countTotalAlbumWaitingApprovalIsApproved($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            $totalApprove = 0;
            $dataResponse = $response->json('data');
            for ($i = 0; $i < count($dataResponse); $i++) {
                $urlGetByIdAlbum = 'http://127.0.0.1:8081/v1/wait_album/view_album/' . $dataResponse[$i]['idAlbum'];
                $responseGetByIdAlbum = Http::withHeaders([
                    'Authorization' => $token,
                    'Accept' => 'application/json',
                ])->get($urlGetByIdAlbum);

                if (isset($responseGetByIdAlbum->json()['data'])) {
                    $dataResponseGetByIdAlbum = $responseGetByIdAlbum->json()['data'];
                    if (isset($dataResponseGetByIdAlbum['approved'])) {
                        $statusApproveddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['approved'];
                        if (($statusApproveddataResponseGetByIdAlbum == true)) {
                            $totalApprove += 1;
                        }
                    }
                }
            }
            return $totalApprove;
        } else {
            return 0;
        }
    }

    private static function countTotalAlbumWaitingApprovalIsRejected($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            $totalRejected = 0;
            $dataResponse = $response->json('data');
            for ($i = 0; $i < count($dataResponse); $i++) {
                $urlGetByIdAlbum = 'http://127.0.0.1:8081/v1/wait_album/view_album/' . $dataResponse[$i]['idAlbum'];
                $responseGetByIdAlbum = Http::withHeaders([
                    'Authorization' => $token,
                    'Accept' => 'application/json',
                ])->get($urlGetByIdAlbum);

                if (isset($responseGetByIdAlbum->json()['data'])) {
                    $dataResponseGetByIdAlbum = $responseGetByIdAlbum->json()['data'];
                    if (isset($dataResponseGetByIdAlbum['rejected'])) {
                        $statusRejecteddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['rejected'];
                        if (($statusRejecteddataResponseGetByIdAlbum == true)) {
                            $totalRejected += 1;
                        }
                    }
                }
            }
            return $totalRejected;
        } else {
            return 0;
        }
    }

    private static function countTotalArtistWaitingApproval($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            return count($response->json()['data']);
        } else {
            return 0;
        }
    }

    private static function countTotalArtistWaitingApprovalIsApproved($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        if (isset($response->json()['data'])) {
            $totalApprove = 0;
            $dataResponse = $response->json('data');
            for ($i = 0; $i < count($dataResponse); $i++) {
                $urlGetByIdArtist = 'http://localhost:8081/v1/wait_artist/view_artist/' . $dataResponse[$i]['idArtist'];
                $responseGetByIdArtist = Http::withHeaders([
                    'Authorization' => $token,
                    'Accept' => 'application/json',
                ])->get($urlGetByIdArtist);

                if (isset($responseGetByIdArtist->json()['data'])) {
                    $dataResponseGetByIdArtist = $responseGetByIdArtist->json()['data'];
                    if (isset($dataResponseGetByIdArtist['approved'])) {
                        $statusApproveddataResponseGetByIdArtist = $dataResponseGetByIdArtist['approved'];
                        if (($statusApproveddataResponseGetByIdArtist == true)) {
                            $totalApprove += 1;
                        }
                    }
                }
            }
            return $totalApprove;
        } else {
            return 0;
        }
    }

    private static function countTotalArtistWaitingApprovalIsRejected($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            $totalRejected = 0;
            $dataResponse = $response->json('data');
            for ($i = 0; $i < count($dataResponse); $i++) {
                $urlGetByIdArtist = 'http://localhost:8081/v1/wait_artist/view_artist/' . $dataResponse[$i]['idArtist'];
                $responseGetByIdArtist = Http::withHeaders([
                    'Authorization' => $token,
                    'Accept' => 'application/json',
                ])->get($urlGetByIdArtist);

                if (isset($responseGetByIdArtist->json()['data'])) {
                    $dataResponseGetByIdArtist = $responseGetByIdArtist->json()['data'];
                    if (isset($dataResponseGetByIdArtist['rejected'])) {
                        $statusRejecteddataResponseGetByIdArtist = $dataResponseGetByIdArtist['rejected'];
                        if (($statusRejecteddataResponseGetByIdArtist == true)) {
                            $totalRejected += 1;
                        }
                    }
                }
            }
            return $totalRejected;
        } else {
            return 0;
        }
    }

    private static function listAlbumWaitingApproval($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            $listAlbum = [];
            $dataResponse = $response->json('data');
            if (count($dataResponse) <= 0) {
                return [];
            } else {
                $orderNo = 0;
                for ($i = 0; $i < count($dataResponse); $i++) {
                    $urlGetByIdAlbum = 'http://localhost:8081/v1/wait_album/view_album/' . $dataResponse[$i]['idAlbum'];
                    $responseGetByIdAlbum = Http::withHeaders([
                        'Authorization' => $token,
                        'Accept' => 'application/json',
                    ])->get($urlGetByIdAlbum);

                    if (isset($responseGetByIdAlbum->json()['data'])) {
                        $dataResponseGetByIdAlbum = $responseGetByIdAlbum->json()['data'];
                        if (isset($dataResponseGetByIdAlbum['rejected']) && isset($dataResponseGetByIdAlbum['approved']) && isset($dataResponseGetByIdAlbum['deleted'])) {
                            $statusApproveddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['approved'];
                            $statusRejecteddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['rejected'];
                            $statusDeleteddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['deleted'];
                            if (($statusRejecteddataResponseGetByIdAlbum == false && $statusApproveddataResponseGetByIdAlbum == false && $statusDeleteddataResponseGetByIdAlbum == false)) {
                                $dataResponse[$i]['order_no'] = $orderNo + 1;
                                array_push($listAlbum, $dataResponse[$i]);
                                $orderNo++;
                            }
                        }
                    }
                }

                if (count($listAlbum) > 4) {
                    return [$listAlbum[0], $listAlbum[1], $listAlbum[2], $listAlbum[3]];
                }
                return $listAlbum;
            }
        } else {
            return [];
        }
    }

    private static function listArtistWaitingApproval($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        if (isset($response->json()['data'])) {
            $listArtist = [];
            $dataResponse = $response->json('data');
            if (count($dataResponse) <= 0) {
                return [];
            } else {
                $orderNo = 0;
                for ($i = 0; $i < count($dataResponse); $i++) {
                    $urlGetByIdArtist = 'http://localhost:8081/v1/wait_artist/view_artist/' . $dataResponse[$i]['idArtist'];
                    $responseGetByIdArtist = Http::withHeaders([
                        'Authorization' => $token,
                        'Accept' => 'application/json',
                    ])->get($urlGetByIdArtist);

                    if (isset($responseGetByIdArtist->json()['data'])) {
                        $dataResponseGetByIdArtist = $responseGetByIdArtist->json()['data'];
                        if (isset($dataResponseGetByIdArtist['rejected']) && isset($dataResponseGetByIdArtist['approved']) && isset($dataResponseGetByIdArtist['deleted'])) {
                            $statusApproveddataResponseGetByIdArtist = $dataResponseGetByIdArtist['approved'];
                            $statusRejecteddataResponseGetByIdArtist = $dataResponseGetByIdArtist['rejected'];
                            $statusDeleteddataResponseGetByIdArtist = $dataResponseGetByIdArtist['deleted'];
                            if (($statusRejecteddataResponseGetByIdArtist == false && $statusApproveddataResponseGetByIdArtist == false && $statusDeleteddataResponseGetByIdArtist == false)) {
                                $dataResponse[$i]['order_no'] = $orderNo + 1;
                                array_push($listArtist, $dataResponse[$i]);
                                $orderNo++;
                            }
                        }
                    }
                }

                if (count($listArtist) > 4) {
                    return [$listArtist[0], $listArtist[1], $listArtist[2], $listArtist[3]];
                }
                return $listArtist;
            }
        } else {
            return [];
        }
    }
}
