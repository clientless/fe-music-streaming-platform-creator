<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ArtistController extends Controller
{
    //
    public function showArtistLive()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Live');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $listArtistLive = self::listArtistLive($auth);

        $message = $listArtistLive['message'];
        $statusCode = $listArtistLive['code'];
        $dataBody = $listArtistLive['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully get object');
        if ($status) {
            return view('pages.artistLive', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function showArtistLiveDetail($id)
    {

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Live Detail');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);
        $artistLiveDetail = self::detailArtistLive($auth, $id);
        $imgArtist = self::imgArtistLive($auth, $id);

        $message = $artistLiveDetail['message'];
        $statusCode = $artistLiveDetail['code'];
        $dataBody = $artistLiveDetail['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get object');
        if ($status) {
            $imgToBase64 = base64_encode($imgArtist);
            return view('pages.artistLiveDetail', compact('dataBody', 'imgToBase64'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function artistLiveDelete(Request $request)
    {
        $requestBody = $request->all();

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Live Delete');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);

        try {
            $artistLiveDetail = self::getArtistLiveDelete($auth,$requestBody);

            $message = $artistLiveDetail['message'];
            $statusCode = $artistLiveDetail['code'];
            $dataBody = $artistLiveDetail['data'];
            $status = ($statusCode == 200 && strtolower($message) == 'successfully to delete');
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Delete User. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }



    public function showArtistWaitingLive()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Waiting Live');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $listArtistLive = self::listArtistWaitingLive($auth);

        $message = $listArtistLive['message'];
        $statusCode = $listArtistLive['code'];
        $dataBody = $listArtistLive['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully get object');
        if ($status) {
            $listArtist = [];
            if (count($dataBody) <= 0) {
                // return [];
                Log::info("Data Adalah array kosong");
            } else {
                $orderNo = 0;
                for ($i = 0; $i < count($dataBody); $i++) {
                    $urlGetByIdArtist = 'http://localhost:8081/v1/wait_artist/view_artist/' . $dataBody[$i]['idArtist'];
                    $responseGetByIdArtist = Http::withHeaders([
                        'Authorization' => $token,
                        'Accept' => 'application/json',
                    ])->get($urlGetByIdArtist);

                    if (isset($responseGetByIdArtist->json()['data'])) {
                        $dataResponseGetByIdArtist = $responseGetByIdArtist->json()['data'];
                        if (isset($dataResponseGetByIdArtist['rejected']) && isset($dataResponseGetByIdArtist['approved']) && isset($dataResponseGetByIdArtist['deleted'])) {
                            $statusApproveddataResponseGetByIdArtist = $dataResponseGetByIdArtist['approved'];
                            $statusRejecteddataResponseGetByIdArtist = $dataResponseGetByIdArtist['rejected'];
                            $statusDeleteddataResponseGetByIdArtist = $dataResponseGetByIdArtist['deleted'];
                            if (($statusRejecteddataResponseGetByIdArtist == false && $statusApproveddataResponseGetByIdArtist == false && $statusDeleteddataResponseGetByIdArtist == false)) {
                                $dataBody[$i]['order_no'] = $orderNo + 1;
                                array_push($listArtist, $dataBody[$i]);
                                $orderNo++;
                            }
                        }
                    }
                }
                $dataBody = $listArtist;
            }
            return view('pages.artistWaitingLive', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function showArtistWaitingLiveDetail($id)
    {

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Waiting Live Detail');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $artistLiveDetail = self::detailArtistWaitingLive($auth, $id);
        $imgArtist = self::imgArtistWaitingLive($auth, $id);

        $message = $artistLiveDetail['message'];
        $statusCode = $artistLiveDetail['code'];
        $dataBody = $artistLiveDetail['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get object');
        if ($status) {
            $imgToBase64 = base64_encode($imgArtist);
            return view('pages.artistWaitingLiveDetail', compact('dataBody', 'imgToBase64'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function approvalArtistWaitingLiveDetailRejected(Request $request)
    {

        // dd($request->all(), $request->selectedIdArtist);
        $id = $request->selectedIdArtist;
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Waiting Live Detail Rejected');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        try {
            $artistLiveDetail = self::detailArtistWaitingLiveRejected($auth, $id, $request->all());

            $message = $artistLiveDetail['message'];
            $statusCode = $artistLiveDetail['code'];
            $dataBody = $artistLiveDetail['data'];
            $status = ($statusCode == 201 && strtolower($message) == 'successfully to change status rejected');
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Reject Approval. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }

    public function approvalArtistWaitingLiveDetailApproved(Request $request)
    {

        $id = $request->selectedIdArtist;
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Artist Waiting Live Detail Approve');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        try {
            $artistLiveDetail = self::detailArtistWaitingLiveApproved($auth, $id, $request->all());

            $message = $artistLiveDetail['message'];
            $statusCode = $artistLiveDetail['code'];
            $dataBody = $artistLiveDetail['data'];
            $status = ($statusCode == 201 && strtolower($message) == 'successfully to change status approve');
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Reject Approval. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }



    private static function listArtistLive($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailArtistLive($authorizationUser, $idArtist)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/artist/view_artist/' . $idArtist;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function imgArtistLive($authorizationUser, $idArtist)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/artist/view_photo/' . $idArtist;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'image/jpeg',
        ])->get($url);

        $bodyResponse = $response->body();

        return $bodyResponse;
    }



    private static function listArtistWaitingLive($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/list_artist';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailArtistWaitingLive($authorizationUser, $idArtist)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/view_artist/' . $idArtist;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function imgArtistWaitingLive($authorizationUser, $idArtist)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/view_photo/' . $idArtist;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'image/jpeg',
        ])->get($url);

        $bodyResponse = $response->body();

        return $bodyResponse;
    }

    private static function detailArtistWaitingLiveRejected($authorizationUser, $idArtist, $request)
    {
        $requestBody = $request;
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/wait_artist/status_approved/artist/' . $idArtist;
        $datas = ['statusReject' => $requestBody['statusReject'], 'statusApprove' => $requestBody['statusApprove']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->put($url, $datas);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailArtistWaitingLiveApproved($authorizationUser, $idArtist, $request)
    {
        $requestBody = $request;
        $token = $authorizationUser;
        // dd($token);
        $url = 'http://localhost:8081/v1/wait_artist/status_approved/artist/' . $idArtist;
        $datas = ['statusReject' => $requestBody['statusReject'], 'statusApprove' => $requestBody['statusApprove']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->put($url, $datas);


        $jsonResponse = $response->json();
        // dd($response)

        return $jsonResponse;
    }
    private static function getArtistLiveDelete($authorizationUser, $request)
    {
        $requestBody = $request;
        // dd($requestBody);

        $idArtist = $requestBody['idArtist'];
        $token = $authorizationUser;
        // dd($token);
        $url = 'http://localhost:8081/v1/artist/delete/by/' . $idArtist;
        $datas = ['deletedBy' => $requestBody['deletedBy'], 'reasonDeleted' => $requestBody['reasonField']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->delete($url, $datas);



        $jsonResponse = $response->json();
        // dd($response)

        return $jsonResponse;
    }
}
