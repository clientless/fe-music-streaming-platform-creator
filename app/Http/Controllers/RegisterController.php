<?php

namespace App\Http\Controllers;

// use App\Http\Requests\RegisterRequest;

use App\Models\MSP\TUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    public function create()
    {
        return view('auth.register');
    }

    public function store()
    {
        $request = request();
        $attributes = $request->validate([
            'fullname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'terms' => 'required'
        ]);

        $userData = [
            'fullname' => $request->fullname,
            'email' => $request->email,
            'password' => $request->password,
        ];

        // DB::beginTransaction();
        try {
            // Make the API request
            $response = Http::post('http://localhost:8082/v1/auth/pic/signup', $userData);

            // Extract response details
            $guzzleResponse = $response->toPsrResponse();
            $statusCode = $guzzleResponse->getStatusCode();
            $responseData = $response->json();
            // Check the API response status code
            if ($statusCode == 201) {
                Log::info("Sukses Masuk");
                $dataResponseApiSuccess = $responseData['data'];
                $insertDataUsers = TUsers::insertToTableUsers($dataResponseApiSuccess, $request);
                return redirect()->route('login')->withSuccess(['message' => $responseData['message']]);
            } else if (!empty($responseData)) {
                return redirect()->back()->withErrors(['message' => $responseData['message']]);
            } else {
                return redirect()->back()->withErrors(['message' => 'Error occurred during registration.']);
            }
            // DB::commit();
        } catch (\Exception $e) {
            Log::error('API Request Error: ' . $e->getMessage());
            DB::rollBack();
            return redirect()->back()->withErrors(['message' => 'Error occurred during registration.']);
        }


    }
}
