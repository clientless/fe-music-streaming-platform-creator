<?php

namespace App\Http\Controllers;

use App\Models\MSP\TUsers;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UserManagement extends Controller
{
    //
    public function showAdmin()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'User Management');

        $username = $userInfo['username'];
        $role = $userInfo['role'];
        $auth = $token[$keyAuth];
        $pageFor = 'Admin';


        $listAdmin = self::listAdmin($auth, $role);
        // dd($listAdmin);
        $message = $listAdmin['message'];
        $statusCode = $listAdmin['code'];
        $dataBody = $listAdmin['data'];
        // $decrypt = Crypt::decrypt();


        if ($statusCode == 200 && strtolower($message) == 'successfully get data') {
            return view('pages.user-management', compact('dataBody','pageFor'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function nonActivateAdmin(Request $request)
    {
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;

        $role = $userInfo['role'];
        $auth = $token[$keyAuth];
        $token = $auth;


        try {
            $url = 'http://localhost:8082/v1/' . strtolower($request->role) . '/' . $request->username;

            $response = Http::withHeaders([
                'Authorization' => $token,
                'Accept' => 'application/json',
            ])->patch($url);

            $jsonResponse = $response->json();

            $message = $jsonResponse['message'];
            $statusCode = $jsonResponse['code'];
            $dataBody = $jsonResponse['data'];
            $status = ($statusCode == 200 && strtolower($message) == 'delete data success');
            // $decrypt = Crypt::decrypt();


            if ($status) {
                // return view('pages.user-management', compact('dataBody'));
                $dataResponseApiSuccess = $dataBody;
                $insertDataUsers = TUsers::updateStatusNonActiveToTableUsers($dataResponseApiSuccess, $request);
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error hapus data. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }

    //
    public function showPic()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'User Management');

        $username = $userInfo['username'];
        $role = $userInfo['role'];
        $auth = $token[$keyAuth];
        $pageFor = 'Pic';


        $listPic = self::listPic($auth, $role);
        // dd($listAdmin);
        $message = $listPic['message'];
        $statusCode = $listPic['code'];
        $dataBody = $listPic['data'];
        // $decrypt = Crypt::decrypt();


        if ($statusCode == 200 && strtolower($message) == 'successfully get data') {
            return view('pages.user-management', compact('dataBody','pageFor'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function nonActivatePic(Request $request)
    {
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;

        $role = $userInfo['role'];
        $auth = $token[$keyAuth];
        $token = $auth;


        try {
            $url = 'http://localhost:8082/v1/' . strtolower($request->role) . '/' . $request->username;

            $response = Http::withHeaders([
                'Authorization' => $token,
                'Accept' => 'application/json',
            ])->patch($url);

            $jsonResponse = $response->json();

            $message = $jsonResponse['message'];
            $statusCode = $jsonResponse['code'];
            $dataBody = $jsonResponse['data'];
            $status = ($statusCode == 200 && strtolower($message) == 'delete data success');
            // $decrypt = Crypt::decrypt();


            if ($status) {
                // return view('pages.user-management', compact('dataBody'));
                $dataResponseApiSuccess = $dataBody;
                $insertDataUsers = TUsers::updateStatusNonActiveToTableUsers($dataResponseApiSuccess, $request);
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error hapus data. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }

        //
        public function showUser()
        {
            //Ambil info dari session
            $userInfo = session()->all()['userinfo'];
            $keyAuth = session()->all()['keyAuth'];
            $token = session()->all()['token'];
            //update pages in session;
            session()->put('page', 'User Management');

            $username = $userInfo['username'];
            $role = $userInfo['role'];
            $auth = $token[$keyAuth];
            $pageFor = 'User';



            $listUser = self::listUser($auth, $role);
            // dd($listAdmin);
            $message = $listUser['message'];
            $statusCode = $listUser['code'];
            $dataBody = $listUser['data'];
            // $decrypt = Crypt::decrypt();


            if ($statusCode == 200 && strtolower($message) == 'successfully get data') {
                return view('pages.user-management', compact('dataBody','pageFor'));
            } else {
                return back()->withErrors(['Error' => $message]);
            }
        }

        public function nonActivateUser(Request $request)
        {
            $userInfo = session()->all()['userinfo'];
            $keyAuth = session()->all()['keyAuth'];
            $token = session()->all()['token'];
            //update pages in session;

            $role = $userInfo['role'];
            $auth = $token[$keyAuth];
            $token = $auth;


            try {
                $url = 'http://localhost:8082/v1/' . strtolower($request->role) . '/' . $request->username;

                $response = Http::withHeaders([
                    'Authorization' => $token,
                    'Accept' => 'application/json',
                ])->patch($url);

                $jsonResponse = $response->json();

                $message = $jsonResponse['message'];
                $statusCode = $jsonResponse['code'];
                $dataBody = $jsonResponse['data'];
                $status = ($statusCode == 200 && strtolower($message) == 'delete data success');
                // $decrypt = Crypt::decrypt();


                if ($status) {
                    // return view('pages.user-management', compact('dataBody'));
                    $dataResponseApiSuccess = $dataBody;
                    $insertDataUsers = TUsers::updateStatusNonActiveToTableUsers($dataResponseApiSuccess, $request);
                    return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
                } else {
                    return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $status = false;
                $dataBody = null;
                Log::info("Error hapus data. Errornya : " . $message);
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        }




    private static function listAdmin($authorizationUser, $roleUser)
    {
        $token = $authorizationUser;
        $role = $roleUser;
        $url = 'http://localhost:8082/v1/admin/list';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();
        return $jsonResponse;
    }

    private static function listPic($authorizationUser, $roleUser)
    {
        $token = $authorizationUser;
        $role = $roleUser;
        $url = 'http://localhost:8082/v1/pic/list';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();
        return $jsonResponse;
    }
    private static function listUser($authorizationUser, $roleUser)
    {
        $token = $authorizationUser;
        $role = $roleUser;
        $url = 'http://localhost:8082/v1/user/list';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();
        return $jsonResponse;
    }
}
