<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AlbumController extends Controller
{
    public function showAlbumLive()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Live');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $listAlbumLive = self::listAlbumLive($auth);

        $message = $listAlbumLive['message'];
        $statusCode = $listAlbumLive['code'];
        $dataBody = $listAlbumLive['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully get object');
        if ($status) {
            return view('pages.albumLive', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function showAlbumLiveDetail($id)
    {

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Live Detail');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);
        $albumLiveDetail = self::detailAlbumLive($auth, $id);
        $imgAlbum = self::imgAlbumLive($auth, $id);

        $message = $albumLiveDetail['message'];
        $statusCode = $albumLiveDetail['code'];
        $dataBody = $albumLiveDetail['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get object: ');
        if ($status) {
            $imgToBase64 = base64_encode($imgAlbum);
            // dd($dataBody);
            return view('pages.albumLiveDetail', compact('dataBody', 'imgToBase64','id'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function albumLiveDelete(Request $request)
    {
        $requestBody = $request->all();

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Live Delete');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth, $id);

        try {
            $albumLiveDetail = self::getAlbumLiveDelete($auth,$requestBody);

            $message = $albumLiveDetail['message'];
            $statusCode = $albumLiveDetail['code'];
            $dataBody = $albumLiveDetail['data'];
            $status = ($statusCode == 200 && strtolower($message) == 'successfully to delete');
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Delete User. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }



    public function showAlbumWaitingLive()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Waiting Live');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        // dd($auth);
        $listAlbumLive = self::listAlbumWaitingLive($auth);

        $message = $listAlbumLive['message'];
        $statusCode = $listAlbumLive['code'];
        $dataBody = $listAlbumLive['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully get object');
        if ($status) {
            $listAlbum = [];
            if (count($dataBody) <= 0) {
                // return [];
                Log::info("Data Adalah array kosong");
            } else {
                $orderNo = 0;
                for ($i = 0; $i < count($dataBody); $i++) {
                    $urlGetByIdAlbum = 'http://127.0.0.1:8081/v1/wait_album/view_album/' . $dataBody[$i]['idAlbum'];
                    $responseGetByIdAlbum = Http::withHeaders([
                        'Authorization' => $token,
                        'Accept' => 'application/json',
                    ])->get($urlGetByIdAlbum);

                    if (isset($responseGetByIdAlbum->json()['data'])) {
                        $dataResponseGetByIdAlbum = $responseGetByIdAlbum->json()['data'];
                        // dd($dataResponseGetByIdAlbum);
                        if (isset($dataResponseGetByIdAlbum['rejected']) && isset($dataResponseGetByIdAlbum['approved']) && isset($dataResponseGetByIdAlbum['deleted'])) {
                            $statusApproveddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['approved'];
                            $statusRejecteddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['rejected'];
                            $statusDeleteddataResponseGetByIdAlbum = $dataResponseGetByIdAlbum['deleted'];
                            if (($statusRejecteddataResponseGetByIdAlbum == false && $statusApproveddataResponseGetByIdAlbum == false && $statusDeleteddataResponseGetByIdAlbum == false)) {
                                $dataBody[$i]['order_no'] = $orderNo + 1;
                                // $dataBody[$i]['idAlbum'] = $orderNo + 1;
                                array_push($listAlbum, $dataBody[$i]);
                                $orderNo++;
                            }
                        }
                    }
                }
                $dataBody = $listAlbum;
                // dd($dataBody);
            }
            return view('pages.AlbumWaitingLive', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function showAlbumWaitingLiveDetail($id)
    {

        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Waiting Live Detail');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        $albumLiveDetail = self::detailAlbumWaitingLive($auth, $id);
        $imgAlbum = self::imgAlbumWaitingLive($auth, $id);

        $message = $albumLiveDetail['message'];
        $statusCode = $albumLiveDetail['code'];
        $dataBody = $albumLiveDetail['data'];
        $status = ($statusCode == 200 && strtolower($message) == 'successfully to get object: ');
        if ($status) {
            $imgToBase64 = base64_encode($imgAlbum);
            return view('pages.albumWaitingLiveDetail', compact('dataBody', 'imgToBase64', 'id'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function approvalAlbumWaitingLiveDetailRejected(Request $request)
    {

        // dd($request->all(), $request->selectedIdAlbum);
        $id = $request->selectedIdAlbum;
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Waiting Live Detail Rejected');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        try {
            $albumLiveDetail = self::detailAlbumWaitingLiveRejected($auth, $id, $request->all());

            $message = $albumLiveDetail['message'];
            $statusCode = $albumLiveDetail['code'];
            $dataBody = $albumLiveDetail['data'];
            $status = ($statusCode == 201 && strtolower($message) == 'successfully to edit status reject');
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Reject Approval. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }

    public function approvalAlbumWaitingLiveDetailApproved(Request $request)
    {

        $id = $request->selectedIdAlbum;
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Album Waiting Live Detail Approve');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];
        try {
            $albumLiveDetail = self::detailAlbumWaitingLiveApproved($auth, $id, $request->all());

            $message = $albumLiveDetail['message'];
            $statusCode = $albumLiveDetail['code'];
            $dataBody = $albumLiveDetail['data'];
            $status = ($statusCode == 201 && strtolower($message) == 'successfully to edit status approve');
            if ($status) {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            } else {
                return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $status = false;
            $dataBody = null;
            Log::info("Error Reject Approval. Errornya : " . $message);
            return response()->json(['status' => $status, 'msg' => $message, 'data' => $dataBody]);
        }
    }


    private static function listAlbumLive($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailAlbumLive($authorizationUser, $idAlbum)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/album/view_album/' . $idAlbum;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function imgAlbumLive($authorizationUser, $idAlbum)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8081/v1/album/view_cover_album/' . $idAlbum;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'image/jpeg',
        ])->get($url);

        $bodyResponse = $response->body();

        return $bodyResponse;
    }


    private static function listAlbumWaitingLive($authorizationUser)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/list_album';

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailAlbumWaitingLive($authorizationUser, $idAlbum)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/view_album/' . $idAlbum;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function imgAlbumWaitingLive($authorizationUser, $idAlbum)
    {
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/view_cover_album/' . $idAlbum;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'image/jpeg',
        ])->get($url);

        $bodyResponse = $response->body();

        return $bodyResponse;
    }

    private static function detailAlbumWaitingLiveRejected($authorizationUser, $idAlbum, $request)
    {
        $requestBody = $request;
        $token = $authorizationUser;
        $url = 'http://127.0.0.1:8081/v1/wait_album/status_approve/album/' . $idAlbum;
        $datas = ['statusReject' => $requestBody['statusReject'], 'statusApprove' => $requestBody['statusApprove']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->put($url, $datas);


        $jsonResponse = $response->json();

        return $jsonResponse;
    }

    private static function detailAlbumWaitingLiveApproved($authorizationUser, $idAlbum, $request)
    {
        $requestBody = $request;
        $token = $authorizationUser;
        // dd($token);
        $url = 'http://127.0.0.1:8081/v1/wait_album/status_approve/album/' . $idAlbum;
        $datas = ['statusReject' => $requestBody['statusReject'], 'statusApprove' => $requestBody['statusApprove']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->put($url, $datas);


        $jsonResponse = $response->json();
        // dd($response)

        return $jsonResponse;
    }

    private static function getAlbumLiveDelete($authorizationUser, $request)
    {
        $requestBody = $request;
        // dd($requestBody);

        $idAlbum = $requestBody['idAlbum'];
        $token = $authorizationUser;
        // dd($token);
        $url = 'http://localhost:8081/v1/album/delete/by/' . $idAlbum;
        $datas = ['deletedBy' => $requestBody['deletedBy'], 'reasonDeleted' => $requestBody['reasonField']];

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->delete($url, $datas);



        $jsonResponse = $response->json();
        // dd($response)

        return $jsonResponse;
    }
}
