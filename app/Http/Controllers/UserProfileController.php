<?php

namespace App\Http\Controllers;

use App\Models\MSP\TUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class UserProfileController extends Controller
{
    public function show()
    {
        //Ambil info dari session
        $userInfo = session()->all()['userinfo'];
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        //update pages in session;
        session()->put('page', 'Profile');

        $username = $userInfo['username'];
        $auth = $token[$keyAuth];

        $detailDataUser = self::detailProfile($auth, $username);
        $message = $detailDataUser['message'];
        $statusCode = $detailDataUser['code'];
        $dataBody = $detailDataUser['data'];
        // $decrypt = Crypt::decrypt();


        if ($statusCode == 200 && strtolower($message) == 'data found') {
            return view('pages.user-profile', compact('dataBody'));
        } else {
            return back()->withErrors(['Error' => $message]);
        }
    }

    public function update(Request $request)
    {
        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        $auth = $token[$keyAuth];

        $attributes = $request->validate([
            'fullname' => 'required',
            'email' => 'required|email',
        ]);

        $userData = [
            'fullname' => $request->fullname,
            'email' => $request->email,
            'password' => "",
            'emailCompany' => "",
        ];

        try {
            // Make the API request
            $response = Http::withHeaders([
                'Authorization' => $auth,
                'Accept' => 'application/json',
            ])->put('http://localhost:8082/v1/admin/' . $request->username, $userData);

            // Extract response details
            $guzzleResponse = $response->toPsrResponse();
            $statusCode = $guzzleResponse->getStatusCode();
            $responseData = $response->json();

            // Check the API response status code
            if ($statusCode == 200) {
                Log::info("Sukses Masuk");
                $dataResponseApiSuccess = $responseData['data'];
                $insertDataUsers = TUsers::updateToTableUsers($dataResponseApiSuccess, $request);
                // return redirect()->route('login')->withSuccess(['message' => $responseData['message']]);
                return back()->withSuccess(['message' => $responseData['message']]);
            } else if (!empty($responseData)) {
                return redirect()->back()->withErrors(['message' => $responseData['message']]);
            } else {
                return redirect()->back()->withErrors(['message' => 'Error occurred during update.']);
            }
        } catch (\Exception $e) {
            Log::error('API Request Error: ' . $e->getMessage());
            DB::rollBack();
            return redirect()->back()->withErrors(['message' => 'Error occurred during update.']);
        }
    }

    public function updatePassword(Request $request)
    {

        $keyAuth = session()->all()['keyAuth'];
        $token = session()->all()['token'];
        $auth = $token[$keyAuth];

        $attributes = $request->validate([
            'password' => 'required',
            'emailCompany' => 'required|email',
        ]);

        // dd("masuk");


        $userData = [
            'password' => $request->password,
            'emailCompany' => $request->emailCompany,
        ];


        try {
            // Make the API request
            $response = Http::withHeaders([
                'Authorization' => $auth,
                'Accept' => 'application/json',
            ])->put('http://localhost:8082/v1/auth/admin/reset', $userData);

            // Extract response details
            $guzzleResponse = $response->toPsrResponse();
            $statusCode = $guzzleResponse->getStatusCode();
            $responseData = $response->json();
            // dd($statusCode, $responseData);

            // Check the API response status code
            if ($statusCode == 200) {
                Log::info("Sukses Masuk");
                $dataResponseApiSuccess = $responseData['data'];
                $insertDataUsers = TUsers::updatePasswordToTableUsers($dataResponseApiSuccess, $request);
                // return redirect()->route('login')->withSuccess(['message' => $responseData['message']]);
                return back()->withSuccess(['message' => $responseData['message']]);
            } else if (!empty($responseData)) {
                return redirect()->back()->withErrors(['message' => $responseData['message']]);
            } else {
                return redirect()->back()->withErrors(['message' => 'Error occurred during update.']);
            }
        } catch (\Exception $e) {
            Log::error('API Request Error: ' . $e->getMessage());
            DB::rollBack();
            return redirect()->back()->withErrors(['message' => 'Error occurred during update.']);
        }
    }


    private static function detailProfile($authorizationUser, $username)
    {
        $token = $authorizationUser;
        $url = 'http://localhost:8082/v1/admin/' . $username;

        $response = Http::withHeaders([
            'Authorization' => $token,
            'Accept' => 'application/json',
        ])->get($url);

        $dataResponse = $response->json();

        return $dataResponse;
    }

    private static function templateResponseDetailNull()
    {
        $data = array();

        $data['id'] = null;
        $data['email'] = '-';
        $data['username'] = '-';
        $data['fullname'] = '-';
        $data['emailCompany'] = '-';
        $data['password'] = '-';
        $data['status'] = false;
        $data['role'] = '-';

        return $data;
    }
}
